hook global BufCreate 	   .*.[.](svelte)         %{
   set-option buffer filetype html
   set-option buffer comment-line //
}
hook global WinSetOption   filetype=javascript    %{
   set-option buffer formatcmd 'prettier'
   set-option buffer matching_pairs '{' '}' '[' ']' '(' ')'
}

hook global WinSetOption   filetype=json  	     %{
   set-option buffer formatcmd 'jq'
   set-option buffer matching_pairs '{' '}' '[' ']' '(' ')'
}
