set global ui_options ncurses_assistant=none
set global ui_options ncurses_enable_mouse=false
set global modelinefmt ''
set global autoinfo onkey
set global autocomplete insert
set global indentwidth 3
set global tabstop 3
set global aligntab true
set global autoreload yes
set global idle_timeout 300

map global normal '#' ': comment-line<ret>'
map global normal /   "/(?i)"

hook global InsertChar     \t                %{ exec -draft h@ }
hook global InsertChar 	   j                 %{ try %{
    exec -draft hH <a-k>jj<ret> d
    exec <esc>
}}

#tweak this
try %{ def find -params 1 -shell-script-candidates  %{ find -type f } %{ edit %arg{1} }}
