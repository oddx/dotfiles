# Highlight word under the cursor
declare-option -hidden regex curword
set-face global CurWord default,rgb:4a4a4a

hook global NormalIdle .*  %{
    evaluate-commands -draft %{ try %{
        execute-keys <space><a-i>w <a-k> '\A\w+\z' <ret>
        set-option buffer curword "%val{selection}"
    } catch %{
        set-option buffer curword ''
    }}
}
add-highlighter global/ dynregex '\b\Q%opt{curword}\E\b' 0:CurWord
