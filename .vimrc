syntax on
imap jj <Esc>
set path=~/work/**
set wildignore+=*/node_modules/*
set undodir=~/.vim/undo
set backspace=indent,eol,start
set belloff=all
set ignorecase
set undofile
set noswapfile
set nowrap
filetype plugin indent on
au BufReadPost *.svelte set syntax=html
